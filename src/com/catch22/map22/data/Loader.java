package com.catch22.map22.data;

import com.catch22.map22.gui.*;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;;

public class Loader
{
	private String filePath;
	private BufferedReader map;
	public String data = "";
	private int numberOfRows, numberOfColumns;

	private void readFile()
	{
		try
		{
			File f = new File(filePath);
			map = new BufferedReader(new FileReader(f));
			char[] charContent = new char[(int)f.length()];
			map.read(charContent);
			String content = new String(charContent);
			map.close();
			
			String[] lines = content.substring(1, content.length() - 1).split("},");
			for(String line : lines)
			{
				String[] numbers = line.substring(2).split(",");
				for(String number : numbers)
					data += number;
				data += "\n";
			}
		}
		catch(Throwable whoTheFuckGivesAFuck)
		{
			System.out.println(whoTheFuckGivesAFuck.getClass().getName());
		}
		
		data = data.substring(0, data.length() - 3);
	}
	
	public Loader(String filePath, int additionals, boolean rows)
	{
		this.filePath = filePath;
		readFile();
		String[] lines = countDimensions();
		String args[] = new String[2];
		args[0] = "10";
		args[1] = rows ? numberOfColumns + "x" + (numberOfRows + additionals) : (numberOfColumns + additionals) + "x" + numberOfRows;
		Console loaded = new Console(args);
		loaded.autoColor(lines);
		loaded.setTitle(filePath);
	}
	
	public Loader(String filePath)
	{
		this.filePath = filePath;
		readFile();
		startConsole();
	}
	
	private void startConsole()
	{
		String[] lines = countDimensions();
		String args[] = new String[2];
		args[0] = "10";
		args[1] = String.valueOf(numberOfColumns) + "x" + numberOfRows;
		Console loaded = new Console(args);
		loaded.autoColor(lines);
		loaded.setTitle(filePath);
	}
	
	private String[] countDimensions()
	{
		String[] lines = data.split("\n");
		numberOfRows = lines.length;
		numberOfColumns = lines[0].length();
		
		return lines;
	}
}