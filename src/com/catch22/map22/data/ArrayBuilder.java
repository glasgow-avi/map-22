package com.catch22.map22.data;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;

import com.catch22.map22.gui.Console;

public class ArrayBuilder
{
	private static int mapWidth, mapHeight;
	
	private static HashMap<Color, Integer> arrayBuilder= new HashMap<>();
	
	public static void writeFile()
	{
		try(BufferedWriter t = new BufferedWriter(new FileWriter("integerArray")))
		{
			t.write("{\n");
			for(int rowIndex = 0; rowIndex < mapHeight; rowIndex++)
			{
				t.write("{");
				for(int colIndex = 0; colIndex < mapWidth; colIndex++)	
					t.write(String.valueOf(arrayBuilder.get(Console.tiles[rowIndex][colIndex].getBackground())) + (colIndex != mapWidth - 1 ? "," : ""));
				t.write("}" + (rowIndex != mapHeight -1 ? "," : "") + "\n");
			}
			t.write("\b\b}");
		}
		catch(Exception t)
		{
			System.out.println("Bras");
		}
	}
	
	/*
	 * BRICKS - 1
	 * WHITE - 0
	 * WATER - -1
	 */
	public ArrayBuilder(int mapWidth, int mapHeight)
	{
		arrayBuilder.put(Color.red, 1);
		arrayBuilder.put(Color.white, 0);
		arrayBuilder.put(Color.blue, -1);
		
		ArrayBuilder.mapWidth = mapWidth;
		ArrayBuilder.mapHeight = mapHeight;
	}	
}