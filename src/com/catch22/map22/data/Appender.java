package com.catch22.map22.data;

import javax.swing.JFrame;

public class Appender
{
	private static JFrame parent;
	
	public static void add(int n, boolean rows)
	{
		new Loader(parent.getTitle(), n, rows);
		parent.dispose();
	}
	
	public Appender(JFrame parent)
	{
		Appender.parent = parent;		
	}
}
