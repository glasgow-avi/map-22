package com.catch22.map22.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import com.catch22.map22.data.*;

public class Menubar
{
	private static JMenuBar menuBar;
	private JMenu file, map, tiles;
	private JMenuItem save, load, appendRows, appendColumns, toggle, reset, floodSelect;
	private JFrame parent;
	
	public Menubar(JFrame parent)
	{
		menuBar = new JMenuBar();
		
		file = new JMenu("File");
		menuBar.add(file);
		map = new JMenu("Map");
		menuBar.add(map);
		tiles = new JMenu("Tiles");
		menuBar.add(tiles);
		
		save = new JMenuItem("Save");
		file.add(save);
		load = new JMenuItem("Load");
		file.add(load);
		appendRows = new JMenuItem("Append Rows");
		map.add(appendRows);
		appendColumns = new JMenuItem("Append Columns");
		map.add(appendColumns);
		toggle = new JMenuItem("Toggle");
		tiles.add(toggle);
		reset = new JMenuItem("Reset");
		tiles.add(reset);
		floodSelect = new JMenuItem("Flood Select");
		tiles.add(floodSelect);
		
		this.parent = parent;
		addActionListeners();
	}
	
	public void addActionListeners()
	{
		save.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				ArrayBuilder.writeFile();
			}
		});
		
		load.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				new Loader(JOptionPane.showInputDialog(parent, "Enter the file path", "Choose File", 1));
				parent.dispose();
			}
		});
		
		appendColumns.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				int input = Integer.valueOf(JOptionPane.showInputDialog(parent, "How many columns would you like to append?", "Append Columns", 3));
				new Appender(parent);
				Appender.add(input, false);
				parent.dispose();
			}
		});
		
		appendRows.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				int input = Integer.valueOf(JOptionPane.showInputDialog(parent, "How many rows would you like to append?", "Append Rows", 3));
				new Appender(parent);
				Appender.add(input, true);
				parent.dispose();
			}
		});
		
		toggle.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				for(int rowIndex = 0; rowIndex < Console.mapHeight; rowIndex++)
						for(int colIndex = 0; colIndex < Console.mapWidth; colIndex++)
							Console.tiles[rowIndex][colIndex].setBackground(Console.tiles[rowIndex][colIndex].getBackground().equals(Color.white) ? Color.red : Color.white);
			}
		});
		
		reset.addActionListener(new ActionListener()

		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				for(int rowIndex = 0; rowIndex < Console.mapHeight; rowIndex++)
						for(int colIndex = 0; colIndex < Console.mapWidth; colIndex++)
							Console.tiles[rowIndex][colIndex].setBackground(Color.white);
			}
		});
		
		floodSelect.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				Console.drag = !Console.drag;
			}
		});
	}
	
	public static JMenuBar getMenuBar()
	{
		return menuBar;
	}
}