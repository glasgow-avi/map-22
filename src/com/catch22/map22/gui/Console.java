package com.catch22.map22.gui;

import com.catch22.map22.data.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Console extends JFrame
{
	private static final long serialVersionUID = 1L;
	
	public static int mapWidth, mapHeight;
	private int tileSize;
	
	public static JButton[][] tiles;
	
	private JPanel tilePanel;
	
	@SuppressWarnings("unchecked")
	public static <T> void printVars(T... a)
	{
		for(T trav : a)
			System.out.print(trav + " ");
		System.out.println();
	}
	
	public void autoColor(String[] data)
	{
		for(int rowIndex = 0; rowIndex < mapHeight; rowIndex++)
		{
			for(int colIndex = 0; colIndex < mapWidth; colIndex++)
			{
				if(data[rowIndex].charAt(colIndex) == '1')
					tiles[rowIndex][colIndex].setBackground(Color.red);
			}
		}
	}
		
	public void inputDimensions(String[] args)
	{
		if(args.length == 2)
		{
			tileSize = Integer.valueOf(args[0]);
			String[] splitMapDimensions = args[1].split("x");
			mapWidth = Integer.valueOf(splitMapDimensions[0]);
			mapHeight = Integer.valueOf(splitMapDimensions[1]);
			
			return ;
		}
		
		tileSize = Integer.valueOf(JOptionPane.showInputDialog(this, "Enter Tile size"));
		
		while(true)
		{
			String mapDimensions = JOptionPane.showInputDialog(this, "Enter map size :: [widthxheight]");
		
			try
			{
				String[] splitMapDimensions = mapDimensions.split("x");
				mapWidth = Integer.valueOf(splitMapDimensions[0]);
				mapHeight = Integer.valueOf(splitMapDimensions[1]);
				break;
			}
			catch(Throwable whoTheFuckGivesAFuck)
			{
				
			}
		}
	}
	
	public Console(String[] args)
	{
		inputDimensions(args);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		
		new ArrayBuilder(mapWidth, mapHeight);
		
		tilePanel = new JPanel();
		tilePanel.setSize(tileSize * mapWidth, tileSize * mapHeight);
		tilePanel.setLayout(new GridLayout(mapHeight, mapWidth));
		
		tiles = new JButton[mapHeight][mapWidth];
		
		for(int rowIndex = 0; rowIndex < mapHeight; rowIndex++)
			for(int colIndex = 0; colIndex < mapWidth; colIndex++)
			{
				tiles[rowIndex][colIndex] = new JButton();
				final JButton tile = tiles[rowIndex][colIndex]; //alias
				tile.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0)
					{
						if(drag == true)
						{
							dragSet.add((JButton) arg0.getSource());
							if(dragSet.size() == 2)
							{
								fill();
								dragSet.clear();
							}
							return;
						}
						dragSet.clear();
						tile.setBackground(new Color(tile.getBackground().equals(Color.red) ? 0xffffff : 0xff0000));
					}
				});
				
				tile.setPreferredSize(new Dimension(tileSize, tileSize));
				tile.setBackground(new Color(0xFFFFFF));
				tilePanel.add(tile);
				tile.setName(String.valueOf(rowIndex) + String.valueOf(colIndex));
			}
	
		tilePanel.setPreferredSize(new Dimension(tileSize * mapWidth, tileSize * mapHeight));
		
		new Menubar(this);
		setJMenuBar(Menubar.getMenuBar());		
		
		add(tilePanel);
		pack();
	}
	
	public static void main(String[] args)
	{
		new Console(args);
		System.err.close();
	}
	
	public static boolean drag = false;
	public static Set<JButton> dragSet= new HashSet<>();
	
	private void fill()
	{
		JButton[] bounds = new JButton[2];
		int buttonIndex = 0;
		for(JButton b : dragSet)
			bounds[buttonIndex++] = b;
			
		int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
		for(int rowIndex = 0; rowIndex < mapHeight; rowIndex++)
			for(int colIndex = 0; colIndex < mapWidth; colIndex++)
			{
				if(bounds[0] == tiles[rowIndex][colIndex])
				{
					x1 = rowIndex;
					y1 = colIndex;
				}
				
				else if(bounds[1] == tiles[rowIndex][colIndex])
				{
					x2 = rowIndex;
					y2 = colIndex;
				}
			}
		
		for(int rowIndex = 0; rowIndex < mapHeight; rowIndex++)
			for(int colIndex = 0; colIndex < mapWidth; colIndex++)
				if(rowIndex >= x1 && colIndex >= y1 && rowIndex <= x2 && colIndex <= y2)
					tiles[rowIndex][colIndex].setBackground(new Color(tiles[rowIndex][colIndex].getBackground().equals(Color.red) ? 0xffffff : 0xff0000));
		
	}
}